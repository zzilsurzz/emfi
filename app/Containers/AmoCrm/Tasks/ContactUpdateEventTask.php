<?php

namespace App\Containers\AmoCrm\Tasks;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Filters\ContactsFilter;
use AmoCRM\Filters\EventsFilter;
use AmoCRM\Filters\LeadsFilter;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Models\EventModel;
use AmoCRM\Models\NoteType\ServiceMessageNote;
use App\Ship\Parents\Task;
use Exception;

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

class ContactUpdateEventTask extends Task
{
    private $apiClient;

    public function __construct(AmoCRMApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function run(array $contacts)
    {

        foreach ($contacts as $contact) {
            $filter = new EventsFilter();
            $filter->setCreatedAt($contact['updated_at']);
            $filter->setEntity([EntityTypesInterface::CONTACTS]);
            $filter->setTypes(['custom_field_value_changed', 'name_field_changed', 'sale_field_changed']);
            $filter->setEntityIds([$contact['id']]);
            try {
                $events = $this->apiClient->events()->get($filter, [EventModel::CONTACT_NAME]);
                foreach ($events as $event) {
                    $type = $event->getType();
                    $field = explode('_', $type)[0];
                    $value = $event->getValueAfter()[0]["{$field}_field_value"][$field] ?? $event->getValueAfter()[0]["{$field}_field_value"]['text'];
                    $name = $event->getEntity()->getName();
                    $updateAt = date('Y-m-d H:i:s', $contact['updated_at']);
        
                    $serviceMessageNote = new ServiceMessageNote();
                    $serviceMessageNote->setEntityId($contact['id'])
                        ->setText("Название контакта: {$name}, Новое значение: {$value}, Дата обновление: {$updateAt}")
                        ->setService('robot')
                        ->setCreatedBy(0);
                    
                    $this->apiClient->notes(EntityTypesInterface::CONTACTS)->addOne($serviceMessageNote);
                }
            } catch (AmoCRMApiException $exception) {
                printError($exception);
                die;
            }
        }

    }
}
