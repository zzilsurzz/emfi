<?php

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Client\LongLivedAccessToken;
use App\Containers\AmoCrm\Configs\App;

use function DI\factory;

return [
    AmoCRMApiClient::class => factory(function (): AmoCRMApiClient {
        $apiClient = new \AmoCRM\Client\AmoCRMApiClient();
        $longLivedAccessToken = new LongLivedAccessToken(App::TOKEN);
        $apiClient->setAccessToken($longLivedAccessToken)
            ->setAccountBaseDomain(App::DOMAIN);
        return $apiClient;
    })
];
