<?php

namespace App\Containers\AmoCrm\Actions;

use AmoCRM\Client\AmoCRMApiClient;
use App\Containers\AmoCrm\Tasks\LeadAddEventTask;
use App\Containers\AmoCrm\Tasks\ContactAddEventTask;
use App\Containers\AmoCrm\Tasks\ContactUpdateEventTask;
use App\Containers\AmoCrm\Tasks\LeadUpdateEventTask;
use App\Ship\Parents\Action;

class WebHooksAction extends Action
{

    private $leadAddEventTask;

    private $leadUpdateEventTask;

    private $contactAddEventTask;

    private $contactUpdateEventTask;

    public function __construct(
        LeadAddEventTask $leadAddEventTask,
        ContactAddEventTask $contactAddEventTask,
        LeadUpdateEventTask $leadUpdateEventTask,
        ContactUpdateEventTask $contactUpdateEventTask
    ) {
        $this->leadAddEventTask = $leadAddEventTask;
        $this->leadUpdateEventTask = $leadUpdateEventTask;
        $this->contactAddEventTask = $contactAddEventTask;
        $this->contactUpdateEventTask = $contactUpdateEventTask;
    }
    public function run(array $data)
    {
        switch (true) {
            case isset($data['leads']['add']):
                $this->leadAddEventTask->run($data['leads']['add']);
                break;
            case isset($data['leads']['update']):
                $this->leadUpdateEventTask->run($data['leads']['update']);
                break;
            case isset($data['contacts']['add']):
                $this->contactAddEventTask->run($data['contacts']['add']);
                break;
            case isset($data['contacts']['update']):
                $this->contactUpdateEventTask->run($data['contacts']['update']);
                break;
            default:
                file_put_contents(__DIR__ . '/test2.txt', 'Метод не найден');
                break;
        }
    }
}
