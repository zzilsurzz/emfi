<?php

namespace App\Containers\AmoCrm\UI\API\Controllers;

use App\Containers\AmoCrm\Actions\WebHooksAction;
use App\Ship\Parents\Controller;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WebHooksController extends Controller
{
    private $action;

    public function __construct(WebHooksAction $action)
    {
        $this->action = $action;
    }

    public function __invoke(Request $request)
    {
        try {
            $data = $request->request->all();
            file_put_contents(__DIR__ . '/log.txt', print_r([$data], 1));
            $this->action->run($data);
        } catch (Exception $e) {
            file_put_contents(__DIR__ . '/log.txt', print_r([$e->getMessage()], 1));
        }
    
        return new Response('');
    }
}
