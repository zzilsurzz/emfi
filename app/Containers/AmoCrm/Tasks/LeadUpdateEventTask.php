<?php

namespace App\Containers\AmoCrm\Tasks;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Filters\EventsFilter;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Models\EventModel;
use AmoCRM\Models\NoteType\ServiceMessageNote;
use App\Ship\Parents\Task;

class LeadUpdateEventTask extends Task
{
    private $apiClient;

    public function __construct(AmoCRMApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function run(array $leads)
    {

        foreach ($leads as $lead) {
            $filter = new EventsFilter();
            $filter->setCreatedAt([$lead['updated_at']]);
            $filter->setEntity([EntityTypesInterface::LEADS]);
            $filter->setTypes(['custom_field_value_changed', 'name_field_changed', 'sale_field_changed']);
            $filter->setEntityIds([$lead['id']]);
            try {
                $events = $this->apiClient->events()->get($filter, [EventModel::CONTACT_NAME]);
                file_put_contents(__DIR__ . '/log.txt', print_r([$events, $filter], 1));
                foreach ($events as $event) {
                    $type = $event->getType();
                    $field = explode('_', $type)[0];
                    $value = $event->getValueAfter()[0]["{$field}_field_value"][$field] ?? $event->getValueAfter()[0]["{$field}_field_value"]['text'];
                    $name = $event->getEntity()->getName();
                    $updateAt = date('Y-m-d H:i:s', $lead['updated_at']);
        
                    $serviceMessageNote = new ServiceMessageNote();
                    $serviceMessageNote->setEntityId($lead['id'])
                        ->setText("Название контакта: {$name}, Новое значение: {$value}, Дата обновление: {$updateAt}")
                        ->setService('robot')
                        ->setCreatedBy(0);
                    
                    $this->apiClient->notes(EntityTypesInterface::LEADS)->addOne($serviceMessageNote);
                }
            } catch (AmoCRMApiException $exception) {
                printError($exception);
                file_put_contents(__DIR__ . '/log.txt', print_r([$exception->getMessage()], 1));

                die;
            }
        }

    }
}
