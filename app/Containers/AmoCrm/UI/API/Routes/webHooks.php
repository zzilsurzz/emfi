<?php

use App\Containers\AmoCrm\UI\API\Controllers\WebHooksController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

return function (RouteCollection $routes) {
    $route = new Route('/api/amo-crm/webhook', ['_controller' => WebHooksController::class]);
    $route->setMethods(['POST', 'GET']);
    $routes->add('api.create.lead', $route);
};
