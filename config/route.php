<?php

use Symfony\Component\Routing\RouteCollection;

return function(RouteCollection $routes) {
    $files = glob(__DIR__ . '/../app/Containers/*/UI/API/Routes/*.php');

    foreach ($files as $file) {
        (include $file)($routes);
    }
};
