<?php

namespace App\Containers\AmoCrm\Tasks;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Models\NoteType\ServiceMessageNote;
use App\Ship\Parents\Task;

class LeadAddEventTask extends Task
{
    private $apiClient;

    public function __construct(AmoCRMApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function run(array $leads)
    {
        foreach ($leads as $lead) {
            $leadModel = $this->apiClient->leads()->getOne($lead['id']);
            $responsibleUserName = $this->apiClient->users()->getOne($leadModel->getResponsibleUserId())->getName();
            $createdAt = date('Y-m-d H:i:s', $leadModel->getCreatedAt());

            $serviceMessageNote = new ServiceMessageNote();
            $serviceMessageNote->setEntityId($lead['id'])
                ->setText("Название сделки: {$leadModel->getName()}, Ответственный: {$responsibleUserName}, Дата создание: {$createdAt}")
                ->setService('robot')
                ->setCreatedBy(0);
            
            $this->apiClient->notes(EntityTypesInterface::LEADS)->addOne($serviceMessageNote);
        }
    }
}
