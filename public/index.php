<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

$rootDir = dirname(__DIR__, 1);
include $rootDir . '/vendor/autoload.php';

/** @var DI\Container */
$container = require $rootDir . '/config/container.php';

$routes = new RouteCollection;
(require $rootDir . '/config/route.php')($routes);

$_SERVER['PATH_INFO'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$context = new RequestContext();
$request = Request::createFromGlobals();
$context->fromRequest($request);

try {
    $matcher = new UrlMatcher($routes, $context);
    $request->attributes->add($matcher->match($context->getPathInfo()));
 
    if ($request->attributes->has('_controller')) {
        $controller = $request->attributes->get('_controller');
        $response = $container->call($controller, [$request]);
        $response->send();
    }
} catch (MethodNotAllowedException $invalidMethod) {
    (new Response('Страница не доступна', 400))->send();
} catch (ResourceNotFoundException $notFound) {
    (new Response('Страница не найдена', 404))->send();
} catch (Exception $e) {
    var_dump($e->getMessage(), $e->getTraceAsString());
}
