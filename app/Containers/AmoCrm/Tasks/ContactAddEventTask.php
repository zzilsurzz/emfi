<?php

namespace App\Containers\AmoCrm\Tasks;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Models\NoteType\ServiceMessageNote;
use App\Ship\Parents\Task;

class ContactAddEventTask extends Task
{
    private $apiClient;

    public function __construct(AmoCRMApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function run(array $contacts)
    {
        foreach ($contacts as $contact) {
            $contactModel = $this->apiClient->contacts()->getOne($contact['id']);
            $responsibleUserName = $this->apiClient->users()->getOne($contactModel->getResponsibleUserId())->getName();
            $createdAt = date('Y-m-d H:i:s', $contactModel->getCreatedAt());

            $serviceMessageNote = new ServiceMessageNote();
            $serviceMessageNote->setEntityId($contact['id'])
                ->setText("Название контакта: {$contactModel->getName()}, Ответственный: {$responsibleUserName}, Дата создание: {$createdAt}")
                ->setService('robot')
                ->setCreatedBy(0);
            
            $this->apiClient->notes(EntityTypesInterface::CONTACTS)->addOne($serviceMessageNote);
        }
    }
}
