<?php

$files = array_merge(
    glob(__DIR__ . '/../app/Containers/*/Configs/dependencies.php') ?: [],
);

$config = array_map(function ($file) {
    return require $file;
}, $files);

return array_merge_recursive(...$config);
